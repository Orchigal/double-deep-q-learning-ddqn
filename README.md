This is a PyTorch implementation of the Double DQN algorithm.

The environment been used to test the algorithm is Pong from OpenAI gym.

Run python main.py --num-processes 24