from collections import deque
import random
import numpy as np
import torch

class replay_buffer(object):
    def __init__(self, max_size):
        self.rep_buff = deque(maxlen=max_size)
        
    def insert(self, state, hx, cx, action, reward, done, next_state, next_hx, next_cx):
        self.rep_buff.append([state.numpy().squeeze(0),
                              hx.numpy().squeeze(0),
                              cx.numpy().squeeze(0),
                              action,
                              reward,
                              done,
                              next_state.numpy().squeeze(0),
                              next_hx.numpy().squeeze(0),
                              next_cx.numpy().squeeze(0)])
        
    def sample(self, batch_size):
        samples = random.sample(self.rep_buff,batch_size)
        states = torch.FloatTensor(np.array(list(np.array(samples)[:,0])))
        hxs = torch.FloatTensor(np.array(list(np.array(samples)[:,1])))
        cxs = torch.FloatTensor(np.array(list(np.array(samples)[:,2])))
        actions = torch.LongTensor(np.array(samples)[:,3].reshape(-1,1).astype('int'))
        rewards = np.array(samples)[:,4].astype('float32').reshape(-1,1)
        dones = np.array(samples)[:,5].astype('int').reshape(-1,1)
        next_states = torch.FloatTensor(np.array(list(np.array(samples)[:,6])))
        next_hxs = torch.FloatTensor(np.array(list(np.array(samples)[:,7])))
        next_cxs = torch.FloatTensor(np.array(list(np.array(samples)[:,8])))
        return states,hxs,cxs,actions,rewards,dones,next_states,next_hxs,next_cxs